import './App.css';
//import react component if madami na ung return
//Ayaw gumana ng Fragment kaya React ang ginamit ko
import React from 'react';
//import navbar
import AppNavbar from './components/AppNavbar';
//import home
import Home from './pages/Home';
//import courses
import Courses from './pages/Courses';
// import container
import { Container } from 'react-bootstrap';


function App() {
  return (
    <React.Fragment>
      <AppNavbar/>
        <Container>
          <Home />
          <Courses />
        </Container>
    </React.Fragment>

  );
}

export default App;
