//Ayaw gumana ng Fragment kaya React ang ginamit ko
import React from 'react';
// import Fragment from 'react';
import Banner from "../components/Banner";
import Highlights from "../components/Highlights";
// import CourseCard from '../components/CourseCard';

// import Welcome from '../components/Welcome';

export default function Home(){
    return(
        <React.Fragment>
            {/* <Welcome name="Jane" age={25} /> */}
            <Banner />
            <Highlights />
            {/* <CourseCard /> */}
        </React.Fragment>
    )
}